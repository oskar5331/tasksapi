package model;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "tasks")
public class Task {

    private enum taskStatus {
        completed, notcompleted
    }

    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "task_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "task_name")
    private String name;

    @Column(name = "task_info")
    private String description;

    @Column(name = "task_state")
    private taskStatus state = taskStatus.notcompleted;

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Task() {
    }

    public void taskDone() {
        state = taskStatus.completed;
    }
}

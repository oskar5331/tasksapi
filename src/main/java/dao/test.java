package dao;

import config.DbConfig;
import model.Task;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class test {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);



        TaskDao dao = ctx.getBean(TaskDao.class);

        dao.insertTask(new Task("jaluta", "jaluta 20 min"));
        dao.insertTask(new Task("jookse", "tiigiring"));
        dao.insertTask(new Task("study", "study for webpage test"));

        System.out.println(dao.getAllTasks());
        Task task = dao.getTaskById(1L);

        task.setDescription("walk for 2km or run 100m");

        Task task2 = dao.getTaskById(3L);

        task2.taskDone();

        dao.insertTask(task);
        dao.insertTask(task2);

//        dao.deleteTaskById(1L);

        System.out.println(dao.getAllTasks());

        dao.deleteAllTasks();

        System.out.println(dao.getAllTasks());

    }

}

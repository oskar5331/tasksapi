DROP SCHEMA PUBLIC CASCADE;

CREATE SEQUENCE task_sequence AS INTEGER START WITH 1;

CREATE TABLE tasks (
  id BIGINT NOT NULL PRIMARY KEY,
  task_name VARCHAR(255),
  task_info VARCHAR(255),
  task_state INTEGER
);